import 'dart:convert';

class SettingsModel {
  final bool sound
  ;
  SettingsModel({
    required this.sound,
  });

  SettingsModel copyWith({
    bool? sound,
  }) {
    return SettingsModel(
      sound: sound ?? this.sound,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'sound': sound,
    };
  }

  factory SettingsModel.fromMap(Map<String, dynamic> map) {
    return SettingsModel(
      sound: map['sound'] ?? false,
    );
  }

  String toJson() => json.encode(toMap());

  factory SettingsModel.fromJson(String source) => SettingsModel.fromMap(json.decode(source));

  @override
  String toString() => 'SettingsModel(sound: $sound)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is SettingsModel &&
      other.sound == sound;
  }

  @override
  int get hashCode => sound.hashCode;
}
