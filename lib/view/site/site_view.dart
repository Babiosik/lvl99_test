import 'package:flutter/material.dart';
import 'package:test_lvl99/utils/one_signal.dart';
import 'package:test_lvl99/utils/remote_config.dart';
import 'package:test_lvl99/widgets/app_bg.dart';
import 'package:webviewx/webviewx.dart';

class SiteView extends StatefulWidget {
  static const String name = 'SiteView';

  const SiteView({super.key});

  @override
  _SiteViewState createState() => _SiteViewState();
}

class _SiteViewState extends State<SiteView> {
  @override
  void initState() {
    OS.sendTag('about');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Site'),
      ),
      body: AppBG(
        child: WebViewX(
          width: size.width,
          height: size.height,
          initialContent: RemoteConfig.link,
        ),
      ),
    );
  }
}
