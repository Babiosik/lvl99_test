import 'package:flame/game.dart';
import 'package:flutter/material.dart';
import 'package:test_lvl99/db/hive_db.dart';
import 'package:test_lvl99/models/scope_model.dart';
import 'package:test_lvl99/view/game/components/game.dart';
import 'package:test_lvl99/widgets/game_end_widget.dart';
import 'package:test_lvl99/widgets/game_pause_menu.dart';

class GameView extends StatefulWidget {
  static const String name = 'GameView';

  const GameView({super.key});

  @override
  _GameViewState createState() => _GameViewState();
}

class _GameViewState extends State<GameView> {
  late MyGame _game;

  @override
  void initState() {
    _game = _createGame();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('GAME'),
        actions: [
          IconButton(
            onPressed: _gamePause,
            icon: Icon(Icons.pause),
          ),
        ],
      ),
      body: GameWidget(game: _game),
    );
  }

  void _gamePause() {
    _game.pauseEngine();
    showDialog(
      context: context,
      builder: (ctx) => Scaffold(
        backgroundColor: Colors.transparent,
        body: Align(
          alignment: Alignment.center,
          child: SizedBox(
            child: GamePauseMenu(
              onCloseTap: () => Navigator.of(context).pop(),
              onContinueTap: () => Navigator.of(context).pop(),
              onNewGameTap: () {
                Navigator.of(context).pop();
                setState(() => _game = _createGame());
              },
            ),
          ),
        ),
      ),
    ).then((value) => _game.resumeEngine());
  }

  MyGame _createGame() {
    return _game = MyGame(
      onGameEnd: (score) {
        HiveDB.addHistory(ScoreModel(name: 'You', score: score));
        showDialog(
          context: context,
          builder: (ctx) => GameEndWidget(
            score: score,
            onNewGame: () {
              Navigator.of(context).pop();
            },
            onBack: () {
              Navigator.of(context).pop();
              Navigator.of(context).pop();
            },
          ),
        ).then((value) {
          setState(() => _game = _createGame());
        });
      },
    );
  }
}
