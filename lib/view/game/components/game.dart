import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flame/parallax.dart';
import 'package:flutter/material.dart';
import 'package:test_lvl99/view/game/components/generator.dart';

import 'space_shuttle.dart';

class MyGame extends FlameGame with HasDraggables, HasCollisionDetection {
  late SpaceShuttle shuttle;

  final void Function(int score) onGameEnd;
  late final double startTime;

  MyGame({required this.onGameEnd}) {
    startTime = currentTime();
  }

  Vector2 get screen => camera.viewport.canvasSize ?? Vector2.zero();

  @override
  Future<void>? onLoad() async {
    shuttle = SpaceShuttle(
      sprite: await loadSprite('space-shuttle.png'),
      position: Vector2(screen.x / 2, screen.y - 100),
      screenSize: screen,
    );
    final paralax = await loadParallaxComponent(
      [
        ParallaxImageData('space-background.png'),
      ],
      baseVelocity: Vector2(0, -200),
      repeat: ImageRepeat.repeat,
    );
    add(paralax);
    add(shuttle);
    add(Generator(
      center: Vector2(screen.x / 2, -100),
      size: Vector2(screen.x / 2, 0),
      sprites: await Future.wait<Sprite>([
        loadSprite('asteroid.png'),
      ]),
    ));
    return super.onLoad();
  }

  void endGame() {
    pauseEngine();
    onGameEnd((currentTime() - startTime).toInt());
  }

  void loadGame() {}
}
