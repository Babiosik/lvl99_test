import 'package:flame/collisions.dart';
import 'package:flame/components.dart';

class EnemyPassive extends SpriteComponent with CollisionCallbacks {
  @override
  bool get debugMode => false;

  double _speed = 0;
  bool _isActive = true;

  EnemyPassive({
    required super.sprite,
    super.position,
    double size = 100,
  }) : super(
          anchor: Anchor.center,
          size: sprite!.originalSize.normalized() * size,
        );

  @override
  Future<void>? onLoad() {
    ShapeHitbox hitbox = CircleHitbox();
    add(hitbox);
    // hitbox.positionType = PositionType.widget;
    return super.onLoad();
  }

  void activate({
    required Vector2 position,
    Sprite? sprite,
    double size = 100,
    double speed = 5,
  }) {
    this.position = position;
    if (sprite != null) {
      this.sprite = sprite;
      this.size = sprite.originalSize.normalized() * size;
    }
    _speed = speed;
    _isActive = true;
  }

  @override
  void update(double dt) {
    if (_isActive) {
      position.add(Vector2(0, _speed));
      if (position.y > 999) {
        _isActive = false;
      }
    }
    super.update(dt);
  }
}
