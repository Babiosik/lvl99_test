import 'package:flame/components.dart';
import 'package:test_lvl99/utils/random.dart';
import 'package:test_lvl99/view/game/components/enemy_passive.dart';

class Generator extends TimerComponent {
  late final List<EnemyPassive> _spawned = [];
  late final Vector2 _center;
  late final Vector2 _size;
  final List<Sprite> sprites;

  int _iterator = 0;

  Generator({Vector2? center, Vector2? size, this.sprites = const []})
      : super(period: 1.5, repeat: true) {
    _center = center ?? Vector2(100, 100);
    _size = size ?? Vector2(100, 100);
  }

  @override
  Future<void>? onLoad() async {
    await super.onLoad();
    for (var i = 0; i < 5; i++) {
      final comp = EnemyPassive(
        position: _center,
        sprite: sprites.first,
      );
      add(comp);
      _spawned.add(comp);
    }
  }

  @override
  void onTick() {
    Vector2 pos = Vector2(
      _center.x + (random.nextDouble() * _size.x * 2 - _size.x),
      _center.y + (random.nextDouble() * _size.y * 2 - _size.y),
    );
    _spawned[_iterator++].activate(position: pos);
    if (_iterator >= _spawned.length) {
      _iterator = 0;
    }
    super.onTick();
  }
}
