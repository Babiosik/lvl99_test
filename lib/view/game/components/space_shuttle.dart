import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flame/input.dart';
import 'package:test_lvl99/view/game/components/enemy_passive.dart';
import 'package:test_lvl99/view/game/components/game.dart';

class SpaceShuttle extends SpriteComponent
    with Draggable, HasGameRef<MyGame>, CollisionCallbacks {
  @override
  bool debugMode = false;

  late final Vector2 screenSize;
  Vector2? dragDeltaPosition;

  SpaceShuttle(
      {Vector2? position, required super.sprite, required Vector2 screenSize})
      : super(
          position: position ?? Vector2.all(100),
          size: Vector2(68.6, 100),
          anchor: Anchor.center,
        ) {
    this.screenSize = Vector2(screenSize.x - 50, screenSize.y - 50);
  }

  @override
  Future<void>? onLoad() {
    ShapeHitbox hitbox = CircleHitbox();
    add(hitbox);
    return super.onLoad();
  }

  @override
  bool onDragStart(DragStartInfo info) {
    dragDeltaPosition = info.eventPosition.game - position;
    return false;
  }

  @override
  bool onDragUpdate(DragUpdateInfo info) {
    final dragDeltaPosition = this.dragDeltaPosition;
    if (dragDeltaPosition == null) {
      return false;
    }

    // border drag possible
    Vector2 result = info.eventPosition.game - dragDeltaPosition;
    if (result.x < 50) {
      result.x = 50;
    } else if (result.x > screenSize.x) {
      result.x = screenSize.x;
    }
    if (result.y < 50) {
      result.y = 50;
    } else if (result.y > screenSize.y) {
      result.y = screenSize.y;
    }

    position.setFrom(result);
    return false;
  }

  @override
  bool onDragEnd(_) {
    dragDeltaPosition = null;
    return false;
  }

  @override
  bool onDragCancel() {
    dragDeltaPosition = null;
    return false;
  }

  @override
  void onCollisionStart(
      Set<Vector2> intersectionPoints, PositionComponent other) {
    if (other is EnemyPassive) {
      gameRef.endGame();
    }
    super.onCollisionStart(intersectionPoints, other);
  }
}
