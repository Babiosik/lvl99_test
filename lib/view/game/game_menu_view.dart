import 'package:flutter/material.dart';
import 'package:test_lvl99/utils/one_signal.dart';
import 'package:test_lvl99/view/game/game_view.dart';
import 'package:test_lvl99/widgets/app_bg.dart';
import 'package:test_lvl99/widgets/game_pause_menu.dart';

class GameMenuView extends StatefulWidget {
  static const String name = 'GameMenuView';

  const GameMenuView({super.key});

  @override
  _GameMenuViewState createState() => _GameMenuViewState();
}

class _GameMenuViewState extends State<GameMenuView> {
  @override
  void initState() {
    OS.sendTag('game');
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AppBG(
        child: Align(
          alignment: Alignment.center,
          child: SizedBox(
            child: GamePauseMenu(
              onCloseTap: () => Navigator.of(context).pop(),
              onNewGameTap: () =>
                  Navigator.of(context).pushNamed(GameView.name),
            ),
          ),
        ),
      ),
    );
  }
}
