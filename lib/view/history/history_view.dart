import 'package:flutter/material.dart';
import 'package:test_lvl99/db/hive_db.dart';
import 'package:test_lvl99/models/scope_model.dart';
import 'package:test_lvl99/widgets/app_bg.dart';
import 'package:test_lvl99/widgets/score_row.dart';

class HistoryView extends StatefulWidget {
  static const String name = 'HistoryView';

  const HistoryView({super.key});

  @override
  _HistoryViewState createState() => _HistoryViewState();
}

class _HistoryViewState extends State<HistoryView> {
  List<ScoreModel> _scores = [];

  @override
  void initState() {
    _scores = HiveDB.getHistory()
      ..sort(
        (a, b) => b.score.compareTo(a.score),
      );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('History'),
      ),
      body: AppBG(
        child: Material(
          color: Colors.blue.withOpacity(0.6),
          child: Scrollbar(
            child: ListView.builder(
              itemCount: _scores.length,
              itemBuilder: (ctx, index) => ScoreRow(item: _scores[index]),
            ),
          ),
        ),
      ),
    );
  }
}
