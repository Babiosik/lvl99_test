import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_lvl99/view/game/game_menu_view.dart';
import 'package:test_lvl99/view/site/site_view.dart';
import 'package:test_lvl99/widgets/app_bg.dart';
import 'package:test_lvl99/widgets/app_elevated_button.dart';

class MainView extends StatefulWidget {
  static const String name = 'MainView';

  const MainView({super.key});

  @override
  _MainViewState createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {
  @override
  Widget build(BuildContext context) {
    return AppBG(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AppElevatedButton(
              text: 'SITE',
              onTap: () => Navigator.of(context).pushNamed(SiteView.name),
            ),
            AppElevatedButton.second(
              text: 'GAME',
              onTap: () => Navigator.of(context).pushNamed(GameMenuView.name),
            ),
          ],
        ),
      ),
    );
  }
}
