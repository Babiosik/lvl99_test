import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:test_lvl99/widgets/app_elevated_button.dart';

class GameEndWidget extends StatelessWidget {
  const GameEndWidget({
    super.key,
    required this.score,
    required this.onNewGame,
    required this.onBack,
  });

  final int score;
  final VoidCallback onNewGame;
  final VoidCallback onBack;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: const Color.fromARGB(255, 132, 199, 255),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
        side: const BorderSide(color: Colors.blue),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Text(
              'Your score',
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            Text(
              '$score',
              style: const TextStyle(
                fontSize: 22,
              ),
            ),
            AppElevatedButton(text: 'New Game', onTap: onNewGame),
            AppElevatedButton(text: 'Back to menu', onTap: onBack),
          ],
        ),
      ),
    );
  }
}
