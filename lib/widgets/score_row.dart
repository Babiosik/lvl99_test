import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:test_lvl99/models/scope_model.dart';

class ScoreRow extends StatelessWidget {
  const ScoreRow({
    super.key,
    required this.item,
  });

  final ScoreModel item;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Row(
        children: [
          Text(
            item.name,
            style: _textStyle,
          ),
          Expanded(
            child: Text(
              '.' * 100,
              maxLines: 1,
              style: _textStyle,
              overflow: TextOverflow.clip,
            ),
          ),
          Text(
            item.score.toString(),
            style: _textStyle,
          )
        ],
      ),
    );
  }

  TextStyle get _textStyle => const TextStyle(
        color: Colors.white,
        fontSize: 18,
      );
}
