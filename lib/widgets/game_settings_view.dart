import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:test_lvl99/db/hive_db.dart';
import 'package:test_lvl99/models/settings_model.dart';
import 'package:test_lvl99/widgets/setting_row.dart';
import 'package:test_lvl99/widgets/toggle.dart';

class GameSettingsDialog extends StatefulWidget {
  const GameSettingsDialog({Key? key}) : super(key: key);

  @override
  State<GameSettingsDialog> createState() => _GameSettingsDialogState();
}

class _GameSettingsDialogState extends State<GameSettingsDialog> {
  late SettingsModel _settings;

  @override
  void initState() {
    _settings = HiveDB.getSettings();
    super.initState();
  }

  @override
  void dispose() {
    HiveDB.setSettings(_settings);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: const Color.fromARGB(255, 132, 199, 255),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
        side: const BorderSide(color: Colors.blue),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SettingRow(
              title: 'Sound',
              value: Toggle(
                initState: _settings.sound,
                onChange: (value) {
                  _settings = _settings.copyWith(sound: value);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
