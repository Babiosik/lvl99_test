import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

class Toggle extends StatefulWidget {
  const Toggle({
    super.key,
    required this.initState,
    this.onChange,
  });

  final bool initState;
  final void Function(bool value)? onChange;

  @override
  State<Toggle> createState() => _ToggleState();
}

class _ToggleState extends State<Toggle> {
  bool _state = false;

  @override
  void initState() {
    _state = widget.initState;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Switch(
      value: _state,
      onChanged: (value) {
        setState(() => _state = value);
        widget.onChange?.call(_state);
      },
    );
  }
}
