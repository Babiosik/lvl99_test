import 'package:flutter/material.dart';

class AppElevatedButton extends StatelessWidget {
  const AppElevatedButton({
    super.key,
    required this.text,
    this.onTap,
    this.style,
  });

  final String text;
  final VoidCallback? onTap;
  final ButtonStyle? style;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onTap,
      style: ButtonStyle(
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
            side: const BorderSide(color: Colors.blue),
          ),
        ),
        padding: MaterialStateProperty.all<EdgeInsets>(
          const EdgeInsets.all(8),
        ),
      ).merge(style),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
            child: Text(
              text,
              style: const TextStyle(fontSize: 18),
            ),
          ),
        ],
      ),
    );
  }

  factory AppElevatedButton.second({
    Key? key,
    required String text,
    VoidCallback? onTap,
    ButtonStyle? style,
  }) =>
      AppElevatedButton(
        text: text,
        onTap: onTap,
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(
            Colors.accents.first,
          ),
        ).merge(style),
      );
}
