import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';

class AppBG extends StatelessWidget {
  const AppBG({
    super.key,
    required this.child,
  });

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: const BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage('assets/images/bg.png'),
        ),
      ),
      child: child,
    );
  }
}
