import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:test_lvl99/view/history/history_view.dart';

import 'app_elevated_button.dart';
import 'game_settings_view.dart';

class GamePauseMenu extends StatelessWidget {
  const GamePauseMenu({
    Key? key,
    this.onCloseTap,
    this.onNewGameTap,
    this.onContinueTap,
    this.onHistoryTap,
    this.onSettingsTap,
  }) : super(key: key);

  final VoidCallback? onCloseTap;
  final VoidCallback? onNewGameTap;
  final VoidCallback? onContinueTap;
  final VoidCallback? onHistoryTap;
  final VoidCallback? onSettingsTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(16),
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Colors.lightBlueAccent.withOpacity(0.2),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              IconButton(
                onPressed: onCloseTap,
                icon: const Icon(
                  Icons.close_rounded,
                  color: Colors.white,
                ),
              ),
            ],
          ),
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 32),
            child: Text(
              'GAME',
              style: TextStyle(
                fontSize: 32,
                fontWeight: FontWeight.w700,
                color: Colors.white,
              ),
            ),
          ),
          AppElevatedButton(
            text: 'New Game',
            onTap: onNewGameTap,
          ),
          AppElevatedButton(
            text: 'Continue',
            onTap: onContinueTap,
          ),
          AppElevatedButton(
            text: 'History',
            onTap: onHistoryTap ??
                () => Navigator.of(context).pushNamed(HistoryView.name),
          ),
          AppElevatedButton(
            text: 'Settings',
            onTap: onSettingsTap ??
                () => showDialog(
                      context: context,
                      builder: (ctx) => const GameSettingsDialog(),
                    ),
          ),
        ],
      ),
    );
  }
}
