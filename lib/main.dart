import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:test_lvl99/app/app.dart';
import 'package:test_lvl99/db/hive_db.dart';
import 'package:test_lvl99/firebase_options.dart';
import 'package:test_lvl99/utils/apps_flyer.dart';
import 'package:test_lvl99/utils/one_signal.dart';
import 'package:test_lvl99/utils/remote_config.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  runZoned(() async {
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );

    await HiveDB.init();
    await RemoteConfig.init();
    await OS.init();
    await AF.init();
    
    runApp(const App());
  });
}
