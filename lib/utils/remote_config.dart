import 'package:firebase_remote_config/firebase_remote_config.dart';

class RemoteConfig {
  static String _link = '';
  static String get link => _link;

  static Future<void> init() async {
    await FirebaseRemoteConfig.instance.fetchAndActivate();
    _link = FirebaseRemoteConfig.instance.getString('link');
  }
}