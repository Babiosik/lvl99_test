import 'package:appsflyer_sdk/appsflyer_sdk.dart';
import 'package:test_lvl99/db/hive_db.dart';

class AF {
  static late AppsflyerSdk _appsflyerSdk;
  static Future<void> init() async {
    _appsflyerSdk = AppsflyerSdk(
      AppsFlyerOptions(afDevKey: 'dyhs7ndXyMZXyCvcV4PPv7'),
    );
    await _appsflyerSdk.initSdk();
    final uid = await _appsflyerSdk.getAppsFlyerUID();
    HiveDB.setAppsFlyerUID(uid ?? '');
  }
}
