

import 'package:onesignal_flutter/onesignal_flutter.dart';

class OS {
  static late OneSignal _oneSignal;
  static Future<void> init() async {
    _oneSignal = OneSignal()..setAppId('b16bf4a1-e539-45b3-8842-86acd914d4b8');
  }

  static void sendTag(String tag) {
    _oneSignal.sendTag(tag, null);
  }
}
