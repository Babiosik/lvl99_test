import 'dart:io';

import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:test_lvl99/models/scope_model.dart';
import 'package:test_lvl99/models/settings_model.dart';

class HiveDB {
  HiveDB._();

  static late final Box<String> _boxSingle;
  static late final Box<String> _boxScore;

  static Future<void> init() async {
    Directory tempDir = await getTemporaryDirectory();
    Hive.init(tempDir.path);
    _boxSingle = await Hive.openBox('single');
    _boxScore = await Hive.openBox('scores');

    if (getHistory().isEmpty) {
      await addHistory(ScoreModel(name: 'Monk', score: 24));
      await addHistory(ScoreModel(name: 'Samurai', score: 35));
    }
  }

  static SettingsModel getSettings() {
    final json = _boxSingle.get('settings') ?? '{}';
    return SettingsModel.fromJson(json);
  }

  static Future<void> setSettings(SettingsModel settings) async {
    await _boxSingle.put('settings', settings.toJson());
  }

  static List<ScoreModel> getHistory() {
    return List<ScoreModel>.from(
      _boxScore.values.map((e) => ScoreModel.fromJson(e)),
    );
  }

  static Future<void> addHistory(ScoreModel score) async {
    await _boxScore.add(score.toJson());
  }

  static String getAppsFlyerUID() {
    return _boxSingle.get('apps_flyer_uid') ?? '';
  }

  static Future<void> setAppsFlyerUID (String uid) async {
    await _boxSingle.put('apps_flyer_uid', uid);
  }
}
