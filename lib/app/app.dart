import 'package:flutter/material.dart';
import 'package:test_lvl99/view/game/game_menu_view.dart';
import 'package:test_lvl99/view/game/game_view.dart';
import 'package:test_lvl99/view/history/history_view.dart';
import 'package:test_lvl99/view/main/main_view.dart';
import 'package:test_lvl99/view/site/site_view.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lvl99 Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: MainView.name,
      routes: {
        MainView.name:(context) => const MainView(),
        SiteView.name:(context) => const SiteView(),
        GameView.name:(context) => const GameView(),
        HistoryView.name:(context) => const HistoryView(),
        GameMenuView.name:(context) => const GameMenuView(),
      },
    );
  }
}
