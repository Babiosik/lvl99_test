# test_lvl99

Test work for Level99

## A Game

### Screenshots

<details>
  <summary>In a menu</summary>
  ![Main menu](/example/s0.png "Main menu")
  ![Game menu](/example/s1.png "Game menu")
  ![History scores](/example/s2.png "History scores")
  ![Settings](/example/s3.png "Settings")
</details>

<details>
  <summary>In a game</summary>
  ![Gameplay](/example/g0.png "Gameplay")
  ![Death](/example/g1.png "Death")
  ![Pause](/example/g2.png "Pause")
</details>

### Download

[Android apk](https://gitlab.com/Babiosik/lvl99_test/-/raw/d83b3498a510589cd1902110eed56648904f7c59/example/release.apk)
